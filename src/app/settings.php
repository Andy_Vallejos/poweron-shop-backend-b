<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    $debug = in_array(filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING), ['127.0.0.1', 'localhost', '::1', null]);
    $ds = DIRECTORY_SEPARATOR;
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'debug' => $debug,
            'displayErrorDetails' => $debug,
            'logger' => [
                'name' => 'poweron-debug',
                'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . $ds . '..' . $ds . '..' . $ds . 'logs' . $ds . 'app.log',
                'level' => Logger::DEBUG,
            ],
            'doctrine' => [
                'entitiesPaths' => [
                    __DIR__ . $ds . '..' . $ds . 'Domain' . $ds . 'Entities'
                ],
                'debug' => $debug,
                'dbParams' => [
                    'driver' => 'pdo_mysql',
                    'user' => $debug ? 'root' : '',
                    'password' => $debug ? '' : '',
                    'dbname' => $debug ? 'shop' : '',
                ]
            ],
            'validation' => [
                'xmlMapping' => __DIR__ . $ds . '..' . $ds . 'Domain' . $ds . 'Validation' . $ds . 'validation.xml'
            ],
            'hashids' => [
                'salt' => '__poweron-salt-2020'
            ]
        ],
    ]);
};
