<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
declare(strict_types=1);

use App\Application\Actions\Product\ListProductsAction;
use App\Application\Actions\Product\ProcessProductAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        return $response;
    });

    $app->group('/api/products', function (Group $group) {
        $group->get('/view', ListProductsAction::class);
        $group->get('/add', ProcessProductAction::class);
        $group->get('/edit/{id:[a-zA-Z0-9]+}', ProcessProductAction::class);
        $group->get('/delete/{id:[a-zA-Z0-9]+}', ProcessProductAction::class);
        $group->get('/{id:[a-zA-Z0-9]+}', ViewUserAction::class);
    });

    $app->group('/api/admin', function (Group $group) {
        // $group->get('', ListUsersAction::class);
        // $group->get('/{id}', ViewUserAction::class);
    });
};
?>
<?php
declare(strict_types=1);

use App\Application\Actions\Product\ListCartsAction;
use App\Application\Actions\Product\ProcessCartsAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        return $response;
    });

    $app->group('/api/carts', function (Group $group) {
        $group->get('/view', ListProductsAction::class);
        $group->get('/add', ProcessProductAction::class);
        $group->get('/remove/{id:[a-zA-Z0-9]+}', ProcessProductAction::class);
        $group->get('/clear/{id:[a-zA-Z0-9]+}', ProcessProductAction::class);
        $group->get('/{id:[a-zA-Z0-9]+}', ViewUserAction::class);
    });

    $app->group('/api/admin', function (Group $group) {
    });
};
?>

<?php
declare(strict_types=1);

use App\Application\Actions\Product\ListBuyAction;
use App\Application\Actions\Product\ProcessBuyAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        return $response;
    });

    $app->group('/api/buy', function (Group $group) {
    });

    $app->group('/api/admin', function (Group $group) {
    });
};
?>
