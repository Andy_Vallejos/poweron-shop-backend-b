<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
declare(strict_types=1);

namespace App\Application\Actions\Product;

use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use App\Domain\Repositories\ProductRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Hashids\Hashids;
abstract class ProductAction extends Action {
    /**
     * @var ProductRepository
     */
    protected $repository;
    /**
     *  @var ValidatorInterface 
     */
    protected $validator;
    /**
     * @var Hashids
     */
    protected $hashids;
    /**
     * @param LoggerInterface $logger
     * @param EntityManager  $entityManager
     */
    public function __construct(LoggerInterface $logger, EntityManager $entityManager, ValidatorInterface $validator, Hashids $hashids) {   
        parent::__construct($logger, $entityManager);
        $this->repository = $entityManager->getRepository('App\Domain\Entities\Product');
        $this->validator = $validator;
        $this->hashids = $hashids;
    }
}
